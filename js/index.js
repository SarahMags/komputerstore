const LAPTOPS = [
    new Laptop(1,
        'Compaq LTE 5280',
        "It has a screen. There's a keyboard. (Some keys might be missing)",
        1500,
        "img/compaq.jpg",
        "This laptop will turn on! It is perfect for the first time user who likes to have a computer around!"),

    new Laptop(2,
        'HP Elitebook 840',
        "Great pc for work. May have some audio issues.",
        3000,
        "img/HP.jpg",
        "This laptop is a laptop. It will top laps magnificently. Microphone is a bit low, but works well with a headset."),

    new Laptop(3,
        'Dell XPS 15',
        "Brilliant 13-inch laptop. Fancy Intel Core CPU.",
        5000,
        "img/dell.jpeg",
        "Great for gaming. This laptop costs less than most smartphones. That makes it feel like a good deal. And it might be."),

    new Laptop(4,
        'Macbook pro 13',
        "It's a 13-inch Macbook. Pricey, but what else do you expect from Apple.",
        13000,
        "img/mac.jpg",
        "Steve Jobs will watch over you if you buy this laptop. We cannot guarantee that it's a good thing.")

];
//FUNCTIONAL CONSTRUCTOR
function Laptop(id, name, features, price, imgSrc, description) {
    this.id = id;
    this.name = name;
    this.features = features;
    this.price = price;
    this.imgSrc = imgSrc;
    this.description = description;
}

//LAPTOP LOGIC
const elLaptopsSelect = document.getElementById('laptops');
const elLaptopFeatures = document.getElementById('laptopFeatures');
const elLaptopImage = document.getElementById('laptopImgSrc');
const elLaptopName = document.getElementById('chosenLaptop');
const elLaptopDescription = document.getElementById("description");
const elLaptopPrice = document.getElementById('laptopPrice');

//Creating laptop elements to add laptops to select dropdown
for (const laptop of LAPTOPS) {
    const elLaptopOption = document.createElement('option');
    elLaptopOption.value = laptop.id;
    elLaptopOption.innerText = laptop.name;
    elLaptopsSelect.appendChild(elLaptopOption);
}
//When changing selected laptops
elLaptopsSelect.addEventListener('change', function (){
    const value = Number(this.value);

    const selectedLaptop = LAPTOPS.find(function (laptop){
        return laptop.id === value;
    })

    //Update laptop information to match selected laptop
    elLaptopFeatures.innerText = selectedLaptop.features;
    elLaptopImage.src = selectedLaptop.imgSrc;
    elLaptopName.innerText = selectedLaptop.name;
    elLaptopDescription.innerText = selectedLaptop.description;
    elLaptopPrice.innerText = selectedLaptop.price + " Kr";
})

//LOAN LOGIC
let balance = 500;
let outstandingLoan = 0;

renderLoan();

//This method prints the bank and loan information.
//It either renders a button to get a loan, or a button to pay the loan
//depending on if there has been given a loan that has not yet been repaid.
function renderLoan() {
    let printBalance = "Balance: " + balance + " Kr.";
    let printLoan = "Outstanding: " + outstandingLoan + " Kr.";

    document.getElementById("balance").innerText = printBalance;
    document.getElementById("loanTxt").innerText = printLoan;

    if (outstandingLoan === 0){
        document.getElementById("loanButton").style.display = "block";
        document.getElementById("outstandingLoan").style.display = "none";
    } else {
        document.getElementById("outstandingLoan").style.display = "block";
        document.getElementById("loanButton").style.display = "none";
    }
}
//Onclick function for button to get a loan.
function loanFunction() {
    let loanPrompt = prompt("Enter desired loan amount. \n" +
        "You cannot get a loan more than double of your bank balance.\n", '0');

    if (loanPrompt === null) { //If user chooses cancel
        return;
    }

    loanPrompt = parseInt(loanPrompt); //Convert input to int

    if (isNaN(loanPrompt)) { //If input is not a number
        alert("Input is not a number!");
        loanFunction();
    } else if (loanPrompt > balance*2){ //If input is more than double of bank balance
        alert("You cannot get a loan more than double your bank balance!");
        loanFunction();
        }else{ //If valid input, add input value to current balance, and add outstanding loan value
        outstandingLoan = loanPrompt;
        balance += loanPrompt;

        renderLoan(); //render new bank and loan values

        alert("Loan given successfully.")
    }
}

//WORK PAY LOGIC
let payBalance = 0;

function workFunction() { //Onclick function for work button.
    payBalance += 100;

    //The following should be a render function
    let printPayBalance = "Pay: " + payBalance + " Kr.";
    document.getElementById("payBalance").innerText = printPayBalance;


}

function bankFunction(){ //Onclick function for bank button. Moves pay balance to bank, 10% is used to pay outstanding
    //loan if there is any.
    if (outstandingLoan === 0) {
        balance += payBalance;
        payBalance = 0;
    }else{
        let tenPercentOfSalary = payBalance / 10;
        outstandingLoan -= tenPercentOfSalary;
        balance += payBalance - tenPercentOfSalary;
        payBalance = 0;
    }

    //This should still be a render function :)
    let printPayBalance = "Pay: " + payBalance + " Kr.";
    document.getElementById("payBalance").innerText = printPayBalance;

    renderLoan();
}

function repayFunction() { //Onclick function for repay button. Used to pay outstanding loan.
    // keeps remaining money in pay if pay balance exceeds outstanding loan.
    if (payBalance > outstandingLoan){
        payBalance -= outstandingLoan;
        outstandingLoan = 0;
    } else {
        outstandingLoan -= payBalance;
        payBalance = 0;

    }

    //This still isn't a render function :/
    let printPayBalance = "Pay: " + payBalance + " Kr.";
    document.getElementById("payBalance").innerText = printPayBalance;

    renderLoan();
}

function buyLaptop(){ //Onclick function for BUY NOW button. Checks if bank balance is enough to buy selected laptop
    let intLaptopPrice = parseInt(elLaptopPrice.innerText);

    if (balance > intLaptopPrice){
        balance -= intLaptopPrice;
        renderLoan();
        alert("Congratulations! \nYou are now the new owner of the "+elLaptopName.innerText)
    }else {
        alert("Get back to work! You can't afford this laptop.")
    }
}

## Komputer Store

### Description
The Komputer Store is a web application created with HTML, CSS 
and Javascript. 
 
The application simulates a web store where you can browse and buy laptops. 
 
It also has a section that works like a bank named "Joe Banker", and a section named "Work",
where the user can work to earn money. 

The user can get a loan up to the double of their bank balance, and they can 
earn money to their pay balance by clicking the "Work" button. The pay
balance can be deposited to the bank by clicking the "Bank" button. If they
have an outstanding loan, 10% of their pay balance will go towards repaying 
that loan when depositing money to the bank. 

Users are only allowed to get one loan at a time, and has to repay that
loan before getting a new one. This is why the "Get a loan" button is 
replaced with a "Repay" button, when they still have an outstanding loan.


When the user has enough money in their bank balance, they can buy a laptop
of their choosing by selecting the laptop, and pressing the "BUY NOW" button. 
  
The select dropdown section controls what laptop information is rendered on the
page. This includes features, an image, description and price.

--- 

Application made by:
Sarah Thorstensen

---
Project repo can be found at: [GitLab](https://gitlab.com/SarahMags/komputerstore)
